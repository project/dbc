<?php

namespace Drupal\dbc\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure settings for this module.
 */
class CssSwitcherSettingForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'dbc.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dbc_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $domain_storage = \Drupal::entityTypeManager()->getStorage('domain');
    $domains = $domain_storage->loadMultiple();
    $config = $this->config(static::SETTINGS);
    foreach ($domains as $domain) {
      $domain_name = $domain->get('name');
      $domain_id = $domain->get('id');
      $form['uploaded_css_uploader_' . $domain_id] = [
        '#type' => 'managed_file',
        '#title' => $this->t('Css for domain @domain_name', ['@domain_name' => $domain_name]),
        '#description' => $this->t('Upload a file..'),
        '#default_value' => $config->get('uploaded_css_uploader_' . $domain_id),
        '#upload_location' => 'public://dbc/' ,
        '#upload_validators' => [
          'file_validate_extensions' => ['css'],
        ],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $domain_storage = \Drupal::entityTypeManager()->getStorage('domain');
    $domains = $domain_storage->loadMultiple();
    $config = $this->config(static::SETTINGS);
    foreach ($domains as $domain) {
      $domain_id = $domain->get('id');
      $this->config(static::SETTINGS)
        ->set('uploaded_css_uploader_' . $domain_id, $form_state->getValue('uploaded_css_uploader_' . $domain_id))
        ->save();
    }

    parent::submitForm($form, $form_state);
  }

}
